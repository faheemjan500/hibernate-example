/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.jcg.hibernate.maven;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.Date;

public class AppMain {

  private static User userObj;
  private static Session sessionObj;

  private static SessionFactory sessionFactoryObj;

  public static void main(String[] args) {

    try {
      sessionObj = buildSessionFactory().openSession();
      sessionObj.beginTransaction();

      for (int i = 106; i <= 111; i++) {
        userObj = new User();
        userObj.setUserid(i);
        userObj.setUsername("Editor " + i);
        userObj.setCreatedBy("Administrator");
        userObj.setCreatedDate(new Date());

        sessionObj.save(userObj);
      }
      System.out.println("\n.......Records Saved Successfully To The Database.......\n");

      // Committing The Transactions To The Database
      sessionObj.getTransaction().commit();
    } catch (final Exception sqlException) {
      sqlException.printStackTrace();
      if (sessionObj.getTransaction() != null) {
        System.out.println("\n.......Transaction Is Being Rolled Back.......");
        sessionObj.getTransaction().rollback();
      }

    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
    }
  }

  private static SessionFactory buildSessionFactory() {

    final Configuration configObj = new Configuration();
    configObj.configure("hibernate.cfg.xml");
    configObj.addAnnotatedClass(User.class);
    final ServiceRegistry serviceRegistryObj =
        new StandardServiceRegistryBuilder().applySettings(configObj.getProperties()).build();

    sessionFactoryObj = configObj.buildSessionFactory(serviceRegistryObj);

    return sessionFactoryObj;
  }
}
